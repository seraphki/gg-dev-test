﻿using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
	public GameObject mainProjectile;
	public GameObject secondaryProjectile;
	
	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			FireProjectile(mainProjectile);
		}

		if (Input.GetMouseButtonDown(1))
		{
			FireProjectile(secondaryProjectile);
		}
	}

	private void FireProjectile(GameObject projectile)
	{
		Vector3 mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePoint.z = 0;
		Quaternion lookRotation = Quaternion.LookRotation(Vector3.forward, mousePoint);

		Instantiate(projectile, transform.position, lookRotation);
	}
}
