﻿using UnityEngine;

public class DamageOnImpactBehaviour : MonoBehaviour, IImpactBehaviour
{
	[SerializeField]
	private int _damageAmount = 0;

	public void OnImpact(GameObject target)
	{
		EntityHealthBehaviour health = target.GetComponent<EntityHealthBehaviour>();

		if (health != null)
		{
			health.Damage(_damageAmount);
		}
	}
}
