﻿using UnityEngine;

public class ScaleOnImpactBehaviour : MonoBehaviour, IImpactBehaviour
{
	public float scaleModifier;

	public void OnImpact(GameObject target)
	{
		EntityScalableBehaviour entityScalable = target.GetComponent<EntityScalableBehaviour>();

		if (entityScalable != null)
		{
			entityScalable.ModifyScale(scaleModifier);
		}
	}
}
