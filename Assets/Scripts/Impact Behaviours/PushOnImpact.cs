﻿using UnityEngine;

public class PushOnImpact : MonoBehaviour, IImpactBehaviour
{
	public float pushDistance;

	public void OnImpact(GameObject target)
	{
		target.transform.position += pushDistance * transform.up;
	}
}