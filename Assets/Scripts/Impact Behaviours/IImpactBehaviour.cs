﻿using UnityEngine;

public interface IImpactBehaviour
{
	void OnImpact(GameObject target);
}
