﻿using UnityEngine;

public class Globals : MonoBehaviour
{
	public const int LAYER_SHOOTABLE = 8;

	public const int LAYERMASK_SHOOTABLE = 1 << LAYER_SHOOTABLE;
}
