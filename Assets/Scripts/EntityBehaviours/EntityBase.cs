﻿using System;
using UnityEngine;

public abstract class EntityBase : MonoBehaviour
{
	private bool _dead = false;
	
	private IEntityBehaviour[] _entityBehaviours;

	private void Awake()
	{
		_entityBehaviours = GetComponentsInChildren<IEntityBehaviour>();

		for (int i = 0; i < _entityBehaviours.Length; i++)
		{
			_entityBehaviours[i].AssignEntity(this);
		}
		
		OnAwake();
	}

	protected virtual void OnAwake()
	{
		
	}

	private void Start()
	{
		OnLifetimeStart();
		
		for (int i = 0; i < _entityBehaviours.Length; i++)
		{
			_entityBehaviours[i].OnLifetimeStart();
		}
	}

	protected virtual void OnLifetimeStart()
	{
		
	}

	private void Update()
	{
		if (_dead)
		{
			OnDeath();
		}
		else
		{
			OnUpdate();
		}
	}

	public void Die()
	{
		_dead = true;
	}

	private void OnUpdate()
	{
		OnLifetimeUpdate();
		
		for (int i = 0; i < _entityBehaviours.Length; i++)
		{
			_entityBehaviours[i].OnLifetimeUpdate();
		}
	}

	protected virtual void OnLifetimeUpdate()
	{
		
	}
	
	private void OnDeath()
	{
		OnLifetimeEnded();
		
		for (int i = 0; i < _entityBehaviours.Length; i++)
		{
			_entityBehaviours[i].OnLifetimeEnded();
		}
		
		Destroy(gameObject);
	}

	protected virtual void OnLifetimeEnded()
	{
		
	}
}

public interface IEntityBehaviour
{
	void AssignEntity(EntityBase entity);

	void OnLifetimeStart();
	void OnLifetimeUpdate();
	void OnLifetimeEnded();
}
