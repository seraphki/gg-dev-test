﻿using UnityEngine;

public class EntityLifetimeBehaviour : MonoBehaviour, IEntityBehaviour
{
	[SerializeField]
	private float _lifetime = 0;

	private EntityBase _entity;
	private float _lifetimeStart;
	
	public void AssignEntity(EntityBase entity)
	{
		_entity = entity;
	}

	public void OnLifetimeStart()
	{
		_lifetimeStart = Time.time;
	}

	public void OnLifetimeUpdate()
	{
		if (Time.time - _lifetimeStart > _lifetime)
		{
			_entity.Die();
		}
	}

	public void OnLifetimeEnded()
	{
	}
}