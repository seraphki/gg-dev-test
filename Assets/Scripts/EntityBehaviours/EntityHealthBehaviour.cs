﻿using UnityEngine;

public class EntityHealthBehaviour : MonoBehaviour, IEntityBehaviour
{
	[SerializeField]
	private int _maxHealth = 0;

	private EntityBase _entity;
	
	public int currentHealth { get; private set; }

	public int maxHealth => _maxHealth;

	public void AssignEntity(EntityBase entity)
	{
		_entity = entity;
	}

	public void OnLifetimeStart()
	{
		currentHealth = _maxHealth;
	}
	
	public void Damage(int damageAmount)
	{
		currentHealth -= damageAmount;

		if (currentHealth <= 0)
		{
			_entity.Die();
		}
	}

	public void OnLifetimeUpdate()
	{
	}

	public void OnLifetimeEnded()
	{
	}
}
