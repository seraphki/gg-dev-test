﻿using UnityEngine;

public class EntityDestroySelfOnCollision : MonoBehaviour, IEntityBehaviour
{
	private EntityBase _entity;
	private bool _markedForDestruction;

	public void AssignEntity(EntityBase entity)
	{
		_entity = entity;
	}
	
	private void OnCollisionEnter2D(Collision2D other)
	{
		_entity.Die();
	}

	public void OnLifetimeStart()
	{
	}

	public void OnLifetimeUpdate()
	{
	}

	public void OnLifetimeEnded()
	{
	}
}
