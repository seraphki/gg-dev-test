﻿using UnityEngine;

public class EntityScalableBehaviour : MonoBehaviour, IEntityBehaviour
{
	private float _scale = 1;

	public void OnLifetimeStart()
	{
		_scale = transform.lossyScale.x;
	}

	public void ModifyScale(float scale)
	{
		_scale *= scale;
		transform.localScale = _scale * Vector3.one;
	}
	
	public void AssignEntity(EntityBase entity)
	{
	}

	public void OnLifetimeUpdate()
	{
	}

	public void OnLifetimeEnded()
	{
	}
}
