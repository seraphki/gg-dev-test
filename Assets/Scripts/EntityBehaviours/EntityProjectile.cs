﻿using System.Collections.Generic;
using UnityEngine;

public class EntityProjectile : EntityBase
{
	private IImpactBehaviour[] _impactBehaviours;
	private ITargetBehaviour[] _targetBehaviours;
	private List<GameObject> _targets = new List<GameObject>();

	protected override void OnAwake()
	{
		_impactBehaviours = GetComponentsInChildren<IImpactBehaviour>();
		_targetBehaviours = GetComponentsInChildren<ITargetBehaviour>();
	}

	protected override void OnLifetimeEnded()
	{
		_targets.Clear();
		
		Impact(transform.position);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		_targets.Clear();
		_targets.Add(other.gameObject);
		
		Impact(other.transform.position);
	}

	public void Impact(Vector3 position)
	{
		for (int i = 0; i < _targetBehaviours.Length; i++)
		{
			_targetBehaviours[i].GatherTargets(position, ref _targets);
		}

		for (int i = 0; i < _targets.Count; i++)
		{
			if (_targets[i] != gameObject)
			{
				Impact(_targets[i]);
			}
		}
	}

	public void Impact(GameObject target)
	{
		for (int i = 0; i < _impactBehaviours.Length; i++)
		{
			_impactBehaviours[i].OnImpact(target);
		}
	}
}
