﻿using UnityEngine;

public class HomingMovementModifier : MonoBehaviour
{
	[SerializeField]
	private float _homingRadius = 0;

	[SerializeField]
	[Range(0, 1)]
	private float _turnSpeed = 0;
	
	private Collider2D[] _hits;

	private const int COLLISION_CAPACITY = 20;
	
	private void Awake()
	{
		_hits = new Collider2D[COLLISION_CAPACITY];
	}

	void Update()
	{
		for (int i = 0; i < _hits.Length; i++)
		{
			_hits[i] = null;
		}
		
		Physics2D.OverlapCircleNonAlloc(transform.position, _homingRadius, _hits, Globals.LAYERMASK_SHOOTABLE);
		float nearestDistance = float.MaxValue;
		EntityBase closestEntity = null;

		for (int i = 0; i < _hits.Length; i++)
		{
			if (_hits[i] == null)
			{
				continue;
			}

			if (_hits[i].gameObject == gameObject)
			{
				continue;
			}

			EntityBase entity = _hits[i].GetComponentInParent<EntityBase>();

			if (entity != null)
			{
				float distance = (entity.transform.position - transform.position).sqrMagnitude;

				if (distance < nearestDistance)
				{
					nearestDistance = distance;
					closestEntity = entity;
				}
			}
		}

		if (closestEntity != null)
		{
			Vector3 targetPosition = closestEntity.transform.position - transform.position;
			Quaternion targetRotation = Quaternion.LookRotation(transform.forward, targetPosition);
			Quaternion rotation = Quaternion.Lerp(transform.rotation, targetRotation, _turnSpeed);

			transform.rotation = rotation;
		}
	}
}
