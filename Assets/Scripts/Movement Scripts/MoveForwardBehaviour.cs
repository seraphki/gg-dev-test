﻿using UnityEngine;

public class MoveForwardBehaviour : MonoBehaviour
{
	[SerializeField]
	[Min(0)]
	private int _speed = 0;

	private void Update()
	{
		transform.position += _speed * Time.deltaTime * transform.up;
	}
}
