﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleAreaTargetBehaviour : MonoBehaviour, ITargetBehaviour
{
	[SerializeField]
	public float _radius = 0;
	
	private Collider2D[] _hits;
	
	private const int COLLISION_CAPACITY = 20;

	private void Awake()
	{
		_hits = new Collider2D[COLLISION_CAPACITY];
	}
	
	public void GatherTargets(Vector3 position, ref List<GameObject> targets)
	{
		for (int i = 0; i < _hits.Length; i++)
		{
			_hits[i] = null;
		}
		
		Physics2D.OverlapCircleNonAlloc(transform.position, _radius, _hits, Globals.LAYERMASK_SHOOTABLE);

		for (int i = 0; i < _hits.Length; i++)
		{
			if (_hits[i] == null)
			{
				continue;
			}

			if (_hits[i].gameObject == gameObject)
			{
				continue;
			}

			if (!targets.Contains(_hits[i].gameObject))
			{
				targets.Add(_hits[i].gameObject);
			}
		}
	}
}
