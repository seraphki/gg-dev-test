﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITargetBehaviour
{
	void GatherTargets(Vector3 position, ref List<GameObject> targets);
}
