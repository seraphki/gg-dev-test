﻿using UnityEngine;

public class ChangeColorByHealth : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer _visualToColor = default;

	[SerializeField]
	private Color _fullHealthColor = Color.white;
	
	[SerializeField]
	private Color _noHealthColor = Color.white;
	
	private EntityHealthBehaviour _health;

	private void Awake()
	{
		_health = GetComponentInParent<EntityHealthBehaviour>();
	}

	private void LateUpdate()
	{
		if (_health != null)
		{
			float delta = (float)_health.currentHealth / _health.maxHealth;
			Color color = Color.Lerp(_noHealthColor, _fullHealthColor, delta);
			_visualToColor.color = color;
		}
	}
}
