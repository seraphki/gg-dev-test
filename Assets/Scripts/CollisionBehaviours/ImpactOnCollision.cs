﻿using UnityEngine;

public class ImpactOnCollision : MonoBehaviour
{
	private IImpactBehaviour[] _impactBehaviours;

	private void Awake()
	{
		_impactBehaviours = GetComponentsInChildren<IImpactBehaviour>();
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		for (int i = 0; i < _impactBehaviours.Length; i++)
		{
			_impactBehaviours[i].OnImpact(other.gameObject);
		}
	}
}
