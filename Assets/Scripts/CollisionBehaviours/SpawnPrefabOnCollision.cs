﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPrefabOnCollision : MonoBehaviour
{
	[SerializeField]
	private GameObject _prefabToSpawn = default;

	[SerializeField]
	private float _prefabScale = 1;

	[SerializeField]
	private bool _destroyAfterTime = false;

	[SerializeField]
	private float _timeToDestruction = 0;

	private void OnCollisionEnter2D(Collision2D other)
	{
		GameObject prefabObj = Instantiate(_prefabToSpawn, transform.position, Quaternion.identity);
		prefabObj.transform.localScale *= _prefabScale;

		if (_destroyAfterTime)
		{
			Destroy(prefabObj, _timeToDestruction);
		}
	}
}
