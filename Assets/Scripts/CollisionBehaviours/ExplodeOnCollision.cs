﻿using UnityEngine;

public class ExplodeOnCollision : MonoBehaviour
{
	[SerializeField]
	private float _explosionRadius = 0;
	
	private IImpactBehaviour[] _impactBehaviours;
	private Collider2D[] _hits;
	
	private const int COLLISION_CAPACITY = 20;

	private void Awake()
	{
		_impactBehaviours = GetComponentsInChildren<IImpactBehaviour>();
		_hits = new Collider2D[COLLISION_CAPACITY];
	}
	
	private void OnCollisionEnter2D(Collision2D other)
	{
		for (int i = 0; i < _hits.Length; i++)
		{
			_hits[i] = null;
		}
		
		Physics2D.OverlapCircleNonAlloc(transform.position, _explosionRadius, _hits, Globals.LAYERMASK_SHOOTABLE);

		for (int i = 0; i < _hits.Length; i++)
		{
			if (_hits[i] == null)
			{
				continue;
			}

			if (_hits[i].gameObject == gameObject)
			{
				continue;
			}
			
			for (int j = 0; j < _impactBehaviours.Length; j++)
			{
				_impactBehaviours[j].OnImpact(_hits[i].gameObject);
			}
		}
	}
}
